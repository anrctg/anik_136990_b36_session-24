<?php

namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;

use App\Model\Database as DB;
if(!isset( $_SESSION)) session_start();


class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;

    public function setData($postVariableData=NULL){
        if(array_key_exists("id", $postVariableData)){
            $this->id = $postVariableData["id"];
        }
        if(array_key_exists("book_title", $postVariableData)){
            $this->book_title = $postVariableData["book_title"];
        }
        if(array_key_exists("author_name", $postVariableData)){
            $this->author_name = $postVariableData["author_name"];
        }
    }
    public function store(){
        $stmt = $this->dbh->prepare("INSERT INTO book_title (title,author) VALUES (:title,:author)");
        $stmt->bindParam(":title",$this->book_title);
        $stmt->bindParam(":author",$this->author_name);
        $msg = $stmt->execute();


        //set message

        if($msg) {
            Message::setMessage("Data inserted succesfully :)");
        }else{
            Message::setMessage("Data inserted faild :(");
        }
    }
//    public function setMessage(){
//        //header("Refresh:0; url=page2.php");
//    }
//






//
//
//
//    public static function setMessage($message){
//        $_SESSION['message']=$message;
//    }
//
//    public static function getMessage(){
//        if(isset($_SESSION['message'])) {
//            $_message = $_SESSION['message'];
//        }
//        else{ $_message='';}
//
//        return $_message;
//
//    }


}