



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<style>
    body{
        padding-top: 20px;
        background-color: #0f0f0f;
        background-image: url("../resource/img/bg.jpg");
        background-size:cover;
        background-repeat: no-repeat;
    }
</style>
<body>


<div class="container">
    <div class="row centered-form text-center">
        <h2 style="color: #1b6d85">Create.php</h2>
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Book Title</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="store.php" method="post">
                        <div class="form-group">
                            <label for="title">Book Title</label>
                            <input type="text" name="book_title" id="first_name" class="form-control input-sm" placeholder="Book Title">
                        </div>
                        <div class="form-group">
                            <label for="author_name">Author</label>
                            <input type="text" name="author_name" id="email" class="form-control input-sm" placeholder="Author name">
                        </div>
                        <input type="submit" value="Create" class="btn btn-info btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
